const USERS_API = 'https://ajax.test-danit.com/api/json/users'
const POSTS_API = 'https://ajax.test-danit.com/api/json/posts'

class Card {
    constructor(postId, title, body, userId, name, email) {
        this.postId = postId
        this.title = title
        this.body = body
        this.userId = userId
        this.name = name
        this.email = email
    }

    // Метод створення картки
    render() {
        const card = document.createElement('div')

        card.id = `${this.postId}`
        card.classList.add('card')
        card.innerHTML = `
            <h2 class="cardTitle">${this.title}</h2>
            <span class="cardText">${this.body}</span>
            <span class="cardUserName">Author: ${this.name}</span>
            <span class="cardUserMail">E-mail: ${this.email}</span>
            <div class="cardBtn">
            <button onclick="deleteCard(${this.postId})">Delete</button>
            </div>
        `
        return card
    }
}

// Функція для видалення картки
function deleteCard(postId) {
    // DELETE-запит на сервер
    fetch(`${POSTS_API}/${postId}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (response.ok) {
                document.getElementById(`${postId}`).remove()
            } else {
                console.waen(`Failed to delete card with postId: ${postId}`)
            }
        })
        .catch(err => console.warn(err))
}

// GET-запити на сервер для отримання списку користувачів та публікацій
fetch(`${USERS_API}`)
    .then(response => response.json())
    .then(users => {
        fetch(`${POSTS_API}`)
            .then(response => response.json())
            .then(posts => {
                // Створення карток публікацій
                const cardsWrapper = document.querySelector('#cardsWrapper')
                posts.forEach(post => {
                    const user = users.find(user => user.id === post.userId)
                    const card = new Card(
                        post.id,
                        post.title,
                        post.body,
                        user.id,
                        user.name,
                        user.email
                    )
                    cardsWrapper.append(card.render())
                })
            })
            .catch(err => console.warn(err))
    })
    .catch(err => console.warn(err))

